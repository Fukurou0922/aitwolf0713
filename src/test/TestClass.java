package test;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class TestClass {

	static Logger logger = Logger.getLogger(TestClass.class.getName());

	public static void main(String[] args) throws SecurityException, IOException {
		FileHandler fileHandler = new FileHandler("logfile.log", false);
		fileHandler.setFormatter(new SimpleFormatter());
		logger.addHandler(fileHandler);
		logger.setLevel(Level.ALL);

		logger.config("naniga"
				+ "eee");
		logger.warning("warnindddg2");

		System.out.println("Test Complete");
	}
}
