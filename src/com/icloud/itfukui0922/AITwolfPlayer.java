/**
 * update()で処理時間が長いとtalk()の呼び出しに答えられずlostClientExceptionが発生する（と思われる）
 * update()で自然言語処理解析を他スレッドに処理を渡し，talk()で同期処理をし，発話生成を行う
 *
 * gameInfo.getRole()をinitialize時点で行うと，サーバとクライアントの同期処理？の問題か，nullExcep.を受ける
 * 	→initializeの時点ではRoleは渡されないのでは？（追記
 *
 * このため，自分自身の役職はcurrentGameInfo.getRole()で適宜受け取る
 *
 * 占い師は即COをする
 * 人狼は狂人が何もしない場合は占い師COをする
 * 狂人は占い師COをし，偽占い結果報告をする
 * 村人は何もしない
 *
 * 一回の発言で複数の話題が入っている場合，解析できるようになった
 * ex: 「私は占い師。彼は人狼だったよ」
 */
package com.icloud.itfukui0922;

import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.aiwolf.common.data.Agent;
import org.aiwolf.common.data.Judge;
import org.aiwolf.common.data.Player;
import org.aiwolf.common.data.Species;
import org.aiwolf.common.data.Talk;
import org.aiwolf.common.net.GameInfo;
import org.aiwolf.common.net.GameSetting;

import com.icloud.itfukui0922.nlp.NatulalLanguageProcessing;
import com.icloud.itfukui0922.nlp.Role;
import com.icloud.itfukui0922.nlp.Topic;
import com.icloud.itfukui0922.strategy.BoardSurface;

public class AITWolfPlayer implements Player {

	/* ログ */
	Logger logger = Logger.getLogger(AITWolfPlayer.class.getName());
	/* スレッド待機時間 */
	final static int THREAD_LIMIT_TIME = 2500;
	/* 同期待ちキュー */
	Queue<Future<Boolean>> futureQueue;
	/* NLPキュー */
	Queue<NatulalLanguageProcessing> nlpQueue;
	/* ゲーム情報 */
	GameInfo currentGameInfo;
	/* talkList読み込みヘッド */
	int talkListHead;
	/* 参加プレイヤー情報 */
	Map<Agent, AgentInfo> agentInfoMap;
	/* 盤面状況 */
	BoardSurface boardSurface;
	/* 発言キュー */
	Queue<String> talkQueue;
	/* 挨拶したか */
	boolean isGreeting;
	/* カミングアウト済みか */
	boolean isCO;
	/* 2日目偽報告「狂人」したか */
	boolean isSecondReport;
	/* 偽報告したプレイヤー「狂人」 */
	Agent lieReport;

	/**
	 * 襲撃対象を返すメソッド 占い師COした人を優先的に狙う．いない場合は生存プレイヤーから適当に返す
	 */
	@Override
	public Agent attack() {
		logger.config("attack()");
		// ----- 占い師COした人がいるのか特定する -----
		for (Agent key : agentInfoMap.keySet()) { // プレイヤー情報を全て調べる
			AgentInfo agentInfo = agentInfoMap.get(key); // プレイヤー情報取得
			Role targetRole = agentInfo.getRole(); // プレイヤーの役職を取得
			if (targetRole.equals(Role.SEER)) { // 占い師COしているか

				Agent agent = agentInfo.getAgent();
				logger.info("Target is " + agent);
				logger.config("==========");
				return agent; // 占い師COした場合はそのエージェントを返す
			}
		}

		// ----- 候補者がいないため，生存プレイヤーから適当にアタック -----
		List<Agent> candidates = currentGameInfo.getAliveAgentList();
		candidates.remove(currentGameInfo.getAgent());
		Agent agent = randomSelect(candidates);

		logger.info("Target is " + agent);
		logger.config("==========");
		return agent;
	}

	/**
	 * 1日の始まりに呼び出されるメソッド 占い師は占い結果の読み込みをおこなう
	 */
	@Override
	public void dayStart() {
		logger.config("dayStart()");
		// ----- 1日目の処理（盤面初期化） -----
		if (currentGameInfo.getDay() == 1) {
			boardSurface = BoardSurface.getInstance(currentGameInfo, agentInfoMap);
		}
		// ----- 各役職の処理 -----
		Judge divInq = null;
		switch (currentGameInfo.getRole()) {
		case SEER: // 占い結果の取り込みと発話生成
			divInq = currentGameInfo.getDivineResult();
			if (divInq != null) {
				Agent target = divInq.getTarget();
				Species result = divInq.getResult();
				AgentInfo playerAgent = agentInfoMap.get(target);
				playerAgent.setTrustly(result == Species.HUMAN ? 1 : 0); // 白判定なら信用度を1に，黒判定なら信用度を0に設定
				// 占い結果の発言
				String resultString = "";
				if (result == Species.HUMAN) { // Speciesの結果を文字列へ変換
					resultString = "人間";
				} else {
					resultString = "人狼";
				}

				String talk = target + "は" + resultString + "でした！"; // 発言文

				talkQueue.offer(talk); // 発言キューに追加
			}
			break;
		default:
			break;
		}
		// ----- talkListHeadのリセット -----
		talkListHead = 0;
		logger.config("==========");
	}

	/**
	 * 占いたいAgentを返す 信用度情報から灰色（0より大きく1より小さい）プレイヤを候補に加え，候補からランダムに決定する． next:
	 * 占い希望を聞いて，その対象を占う
	 */
	@Override
	public Agent divine() {
		logger.config("divine()");
		// ----- 占い師RCO -----
		if (!isCO) { //
			isCO = true;
			talkQueue.offer("私が占い師です！");
		}

		List<Agent> candidates = new ArrayList<>();

		// 灰色から占いを行う
		for (Agent key : agentInfoMap.keySet()) { // 全てのエージェント捜査
			float trustly = agentInfoMap.get(key).getTrustly();
			if (trustly != Float.MAX_VALUE && trustly != Float.MIN_VALUE) {
				candidates.add(agentInfoMap.get(key).getAgent()); // 候補に加える
			}
		}

		// 候補者がいない場合は誰も占わない
		if (candidates.isEmpty()) {
			return null;
		}

		// 対象者を占う
		Agent agent = randomSelect(candidates);
		logger.info("Target is " + agent);
		logger.config("==========");
		return agent;
	}

	@Override
	public void finish() {
		logger.config("finish()");
		logger.config("==========");
	}

	/**
	 * プレイヤ名を返すだけ
	 */
	@Override
	public String getName() {
		logger.config("getName()");
		logger.config("==========");
		return "AITWolf";
	}

	/**
	 * このメソッドは使われない（未実装）
	 */
	@Override
	public Agent guard() {
		logger.config("guard()");
		logger.config("==========");
		return null;
	}

	/**
	 * 初期化
	 */
	@Override
	public void initialize(GameInfo gameInfo, GameSetting gameSetting) {
		// ## ログ設定 ##
		try {
			Calendar calendar = Calendar.getInstance();
			String logFileName = "log/" + calendar.getTime() + "LoggerLog.txt";
			FileHandler fileHandler = new FileHandler(logFileName, false);
			fileHandler.setFormatter(new SimpleFormatter());
			logger.addHandler(fileHandler);
			logger.setLevel(Level.ALL);
		} catch (SecurityException | IOException e) {
			System.err.println("ログ設定でエラー");
			e.printStackTrace();
		}
		// ## ##

		logger.config("initialize()"); // ログファイルに出力されない（理由不明）
		// ----- フィールドの初期化 -----
		futureQueue = new ArrayDeque<>();
		nlpQueue = new ArrayDeque<>();
		currentGameInfo = gameInfo;
		isCO = false;
		talkListHead = 0;
		agentInfoMap = new HashMap<>();
		talkQueue = new ArrayDeque<>();
		isGreeting = false;
		isSecondReport = false;

		// ----- 配列内のゴミを削除 -----
		nlpQueue.clear();
		agentInfoMap.clear();
		talkQueue.clear();

		// --- 全ての参加エージェントの取得 ---
		for (Agent agent : gameInfo.getAgentList()) {
			// 自分自身はプレイヤーリストに入れない
			if (agent == currentGameInfo.getAgent()) {
				continue;
			}
			AgentInfo playerAgent = new AgentInfo(agent);
			agentInfoMap.put(agent, playerAgent);
		}

		logger.config("==========");
	}

	/**
	 * talk()は次の3つに分割される 1. 解析 時間がかかるためにtalk()に処理を委託しているものを処理
	 * 発言することがあればキューに入れて３．へ 3. 発言 キューにあるものを順々に発言する
	 */
	@Override
	public String talk() {
		double start = System.nanoTime();
		logger.config("talk()");
		// ----- 0日目処理 -----
		if (currentGameInfo.getDay() == 0) {
			if (isGreeting) {
				return Talk.OVER;
			} else {
				isGreeting = true;
				return "よろしくお願いします!";
			}
		}
		if (!isCO && currentGameInfo.getRole().equals(org.aiwolf.common.data.Role.POSSESSED)) { // 狂人の場合は偽占CO
			isCO = true;
			talkQueue.offer("私が占い師です！");
			List<Agent> agentList = currentGameInfo.getAliveAgentList();
			agentList.remove(currentGameInfo.getAgent());
			Agent agent = randomSelect(agentList);
			talkQueue.offer(agent + "は白でした！"); // 偽報告
			lieReport = agent;
		}
		// ----- 2日目処理 -----
		if (currentGameInfo.getDay() == 2) {
			if (!isSecondReport && currentGameInfo.getRole().equals(org.aiwolf.common.data.Role.POSSESSED)) { // 狂人の場合
				List<Agent> agentList = currentGameInfo.getAliveAgentList();
				agentList.remove(currentGameInfo.getAgent());
				agentList.remove(lieReport);
				Agent agent = randomSelect(agentList);
				talkQueue.offer(agent + "は白でした！");	// 偽報告
			}
		}

		// nlpQueueにある内容よりプレイヤー情報を更新
		while (!nlpQueue.isEmpty())

		{
			NatulalLanguageProcessing nlp = nlpQueue.poll();
			Future<Boolean> future = futureQueue.poll();
			// 解析が完了していない場合は解析完了までスレッド待機（THREAD_LIMIT_TIMEミリ秒まで待つ）
			try {
				if (future.get(THREAD_LIMIT_TIME, TimeUnit.MILLISECONDS)) {
					// カミングアウトがあればプレイヤー情報を更新
					Map<String, Topic> topics = nlp.getTopics();
					for (String key : topics.keySet()) {
						if (topics.get(key).equals(Topic.COMINGOUT)) {
							AgentInfo agentInfo = agentInfoMap.get(nlp.getTalk().getAgent());
							if (agentInfo == null) {
								System.out.println("null");
							}
							agentInfo.setRole(nlp.getRoles().get(key));
						}
					}
				}
			} catch (TimeoutException e) {
				System.err.println("解析が完了しませんでした．THREAD_LIMIT_TIMEを調整してください．");
			} catch (InterruptedException | ExecutionException e) {
				System.err.println("同期処理で予期しないエラーが発生");
			}
		}

		// 盤面アラート（自分占他人占COした時に抗議するなど）
		// ここまでの処理を計測しログファイル出力
		double end = System.nanoTime();
		double time = (end - start) / 1000000;
		logger.info("talk() time spent:" + time + "ms");

		// 発言キューが空になるまで発言し続ける
		if (!talkQueue.isEmpty()) {
			String talk = talkQueue.poll();
			logger.info("Said that " + talk);
			logger.config("==========");
			return talk;
		}
		logger.config("==========");
		return Talk.OVER;
	}

	@Override
	public void update(GameInfo gameInfo) {
		logger.config("update()");
		// currentGameInfo をアップデート
		currentGameInfo = gameInfo;

		// GameInfo.talkListからカミングアウト・占い報告・霊媒報告を抽出
		for (int i = talkListHead; i < currentGameInfo.getTalkList().size(); i++) {
			// 発話内容取得
			Talk talk = currentGameInfo.getTalkList().get(i);
			// 自然言語解析のインスタンス生成
			NatulalLanguageProcessing natulalLanguageProcessing = new NatulalLanguageProcessing(currentGameInfo, talk);
			// 自然言語解析のスレッド開始
			ExecutorService executorService = Executors.newCachedThreadPool();
			Future<Boolean> future = executorService.submit(natulalLanguageProcessing);
			// 同期処理のためのキュー
			futureQueue.add(future);
			// nlpキューへの追加
			nlpQueue.add(natulalLanguageProcessing);
		}
		talkListHead = currentGameInfo.getTalkList().size();
		// ログ出力
		logger.config("==========");
	}

	/**
	 * 信用度が低いものから投票をする
	 */
	@Override
	public Agent vote() {
		logger.config("vote()");
		Agent voteAgent = null; // 投票先エージェント

		// 信用度が低い生きているエージェントに対して投票をおこなう
		for (Agent key : agentInfoMap.keySet()) { // 全てのエージェントを走査
			float lowestTrustly = 1; // 最も低い信用度を図るための一時変数
			AgentInfo playerAgent = agentInfoMap.get(key); // 走査対象
			if (!playerAgent.isAlive) { // 追放されている場合は除外
				continue;
			}
			if (playerAgent.getTrustly() < lowestTrustly) { // 信用度が最も低いエージェントを投票先へ
				voteAgent = agentInfoMap.get(key).getAgent();
			}
		}
		logger.info("Target is" + voteAgent);
		logger.config("==========");
		return voteAgent;
	}

	/**
	 * 5人対戦のためこのメソッドは呼ばれない（未実装）
	 */
	@Override
	public String whisper() {
		logger.config("whisper()");
		logger.config("==========");
		return null;
	}

	/**
	 * リストからランダムに選んで返す
	 *
	 * @param list
	 *            リスト（空リストも含む）
	 * @return リストから一つの要素をランダムに返す（空リストの場合はnullを返す）
	 */
	<T> T randomSelect(List<T> list) {
		if (list.isEmpty()) {
			return null;
		} else {
			return list.get((int) (Math.random() * list.size()));
		}
	}

	/**
	 * このメソッドを呼び出したメソッドの呼び出し元のメソッド名、ファイル名、行数の情報を取得します
	 *
	 * @return メソッド名、ファイル名、行数の情報文字列
	 */
	private static String calledFrom() {
		StackTraceElement[] steArray = Thread.currentThread().getStackTrace();
		if (steArray.length <= 3) {
			return "";
		}
		StackTraceElement ste = steArray[3];
		StringBuilder sb = new StringBuilder();
		sb.append(ste.getMethodName()) // メソッド名取得
				.append("(").append(ste.getFileName()) // ファイル名取得
				.append(":").append(ste.getLineNumber()) // 行番号取得
				.append(")");
		return sb.toString();
	}

	/*
	 * ログ出力する際に定型文を返したいためmethodにしておく
	 *
	 * @return ログに出力する文字列
	 */
	String getBasicLogString() {
		return " PlayerName: " + currentGameInfo + " Method: " + calledFrom() + "\t|";
	}
}
