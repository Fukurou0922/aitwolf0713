/**
 * このクラスはプラットフォームのenum型と自作したenum型の変換をするためのクラス
 */
package com.icloud.itfukui0922.util;

public class Conversion {

	/**
	 * プラットフォームのRoleと自作のRoleを変換する
	 * @param plaRole
	 * @return
	 */
	public static com.icloud.itfukui0922.nlp.Role roleToRole (org.aiwolf.common.data.Role plaRole) {
		com.icloud.itfukui0922.nlp.Role oriRole = com.icloud.itfukui0922.nlp.Role.getRole(plaRole.toString());
		return oriRole;
	}
}
