/**
 * Singleton（インスタンスはgetInstanceで取得）
 * ゲームの盤面を管理
 * 自身のエージェントのフラグ，ゲーム状況の保持
 *
 */
package com.icloud.itfukui0922.strategy;

import java.util.Map;

import org.aiwolf.common.data.Agent;
import org.aiwolf.common.net.GameInfo;

import com.icloud.itfukui0922.AgentInfo;
import com.icloud.itfukui0922.nlp.Role;

public class BoardSurface {

	/* ゲーム情報 */
	GameInfo currentGameInfo;
	/* プレイヤー情報 */
	Map<Agent, AgentInfo> agentInfoMap;
	/* 自分自身の役職 */
	Role myRole;
	/* カミングアウト済みか */
	boolean isCO = false;

	/* シングルトン */
	private static BoardSurface boardSurface = new BoardSurface();
	private BoardSurface(){}
	public static BoardSurface getInstance(GameInfo gameInfo, Map<Agent, AgentInfo> agentInfoMap) {
		boardSurface.currentGameInfo = gameInfo;
		boardSurface.agentInfoMap = agentInfoMap;
		boardSurface.myRole = Role.getRole(gameInfo.getRole().toString());
		return boardSurface;
	}

	public boolean getIsCO () {
		return isCO;
	}

	public void setIsCO (boolean isCO) {
		this.isCO = isCO;
	}

	/**
	 *  プレイヤー情報の更新と盤面更新
	 * @param agentInfoMap
	 */
	public void update(GameInfo gameInfo, Map<Agent, AgentInfo> agentInfoMap) {
		this.currentGameInfo = gameInfo;
		this.agentInfoMap = agentInfoMap;
	}

	/**
	 * 占い師の数を返す
	 */
	public int countSeer () {
		int count = 0;
		for (Agent key : agentInfoMap.keySet()) {
			AgentInfo agentInfo = agentInfoMap.get(key);
			Role role = agentInfo.getRole();
			if (role.equals(Role.SEER)) {
				count++;
			}
		}
		return count;
	}

	/**
	 * 自分自身と同じ役職を持っているエージェントを返す
	 * いない場合はnullを返す
	 * @return　自分自身と同じ役職をCOしたAgentまたはnull
	 */
	public Agent sameRole () {
		for (Agent key : agentInfoMap.keySet()) {
			AgentInfo agentInfo = agentInfoMap.get(key);
			Role targetRole = agentInfo.getRole();
			if (myRole.equals(targetRole)) {
				return agentInfo.getAgent();
			}
		}
		return null;
	}


}
