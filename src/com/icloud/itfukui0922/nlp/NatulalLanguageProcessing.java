/**
 * 自然言語処理を扱うクラス
 *
 * Callが返す値が，解析完了したかどうかを返すBooleanというのは少し勿体無い気がする
 */
package com.icloud.itfukui0922.nlp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.aiwolf.common.data.Talk;
import org.aiwolf.common.net.GameInfo;

import com.icloud.itfukui0922.nlp.svm.SVM;

public class NatulalLanguageProcessing implements Callable<Boolean> {

	/* ゲーム情報 */
	GameInfo currentGameInfo;
	/* フィルター情報 */
	static List<String> filterList = new ArrayList<>();
	/* Talk型 */
	volatile Talk talk;
	/* 発言を文を分解 addで追加すること */
	volatile List<String> separateTalks;
	/* 発言に含まれいていた話題（複数可能) Topic.UNTAGで初期化 */
	volatile Map<String, Topic> topics;
	/* 発言に含まれていた役職名（複数可能) Role.NOTROLEで初期化*/
	volatile Map<String, Role> roles;
	/* 解析不要フラグ */
	volatile boolean analysisNotNeed = false;

	static {
		// フィルタ情報の読み込み
		try {
			File csv = new File("filterInformation.txt");

			BufferedReader bufferedReader = new BufferedReader(new FileReader(csv));
			String readLine;
			while ((readLine = bufferedReader.readLine()) != null) {
				filterList.add(readLine);
			}
			bufferedReader.close();
		} catch (FileNotFoundException e) {
			System.err.println("フィルタ情報読み込みでエラー" + e);
		} catch (IOException e) {
			System.err.println("フィルタ情報読み込みでエラー" + e);
		}
	}

	public Talk getTalk() {
		return talk;
	}

	public Map<String, Topic> getTopics() {
		return topics;
	}

	public Map<String, Role> getRoles() {
		return roles;
	}



	public boolean getAnalysisNotNeed() {
		return analysisNotNeed;
	}

	/**
	 * コンストラクタ
	 */
	public NatulalLanguageProcessing(GameInfo currentGameInfo, Talk talk) {
		this.currentGameInfo = currentGameInfo;
		this.talk = talk;
		topics = new HashMap<>();
		roles = new HashMap<>();
	}

	/**
	 * 言語解析開始
	 *
	 * 1.フィルタリング（雑談など解析不要な文を取り除く） 自分自身の発話，SKIP，OVERなど 2.文の分割 3.話題解析
	 * 4.話題別解析（誰に投票するか，なんの役職をCOしたのか，．．．）
	 */
	@Override
	public Boolean call() throws Exception {
		// ----- フィルタリング１ -----
		String text = talk.getText();
		if (talk.getAgent() == currentGameInfo.getAgent() || text.equals("") || text.equals("Over")
				|| text.equals("Skip") || isChat(text)) {
			// 解析する必要のない発話を除外（自分自身の発言，中身のない発言，Over・Skip発言，雑談

			analysisNotNeed = true;
			return true;
		}

		// ----- 文の分割 -----
		separateTalks = new ArrayList<>(separateText(text));
		// ----- TopicとRoleの初期化 -----
		for (String string : separateTalks) {
			initTopicsAndRoles(string);
		}

		// ----- 話題解析 -----
		ExecutorService executorService = Executors.newSingleThreadExecutor();
		for (String string : separateTalks) {
			if (isChat(string)) {
				continue;
			}
			Future<String> future = executorService.submit(new SVM(Classifier.Topic, string));
			try {
				// ここでロジックエラー発生する可能性がある
				Topic topic = Topic.getTopic(future.get());
				topics.put(string, topic);
			} catch (InterruptedException | ExecutionException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}
		}

		// ----- 話題別解析 -----

		for (String key : separateTalks) {
			Topic topic = topics.get(key);
			if (topic == null) {
				continue;
			}
			switch (topic) {
			case COMINGOUT:
				roles.put(key, roleCheck(key)); // 役職
				break;
			case DIVINED:

				break;
			default:
				break;
			}

		}

		// 解析完了
		return true;
	}

	/**
	 * 一文になんの単語があるかで役職を返す
	 */
	public Role roleCheck(String text) {
		// 特定の単語があるかで役職を返す
		if (text.indexOf("占") != -1) {
			return Role.SEER;
		} else if (text.indexOf("狼") != -1) {
			return Role.WEREWOLF;
		} else if (text.indexOf("村") != -1) {
			return Role.VILLAGER;
		} else if (text.indexOf("裏") != -1) {
			return Role.POSSESSED;
		} else if (text.indexOf("狂") != -1) {
			return Role.POSSESSED;
		} else if (text.indexOf("者") != -1) {
			return Role.POSSESSED;
		}

		return Role.NOTROLE;
	}

	/*
	 * 雑談かどうかを判定
	 */
	private boolean isChat(String text) {
		for (String filter : filterList) {
			if (text.indexOf(filter) != -1) {
				return false;
			}
		}
		return true;
	}

	/*
	 * 文の分割
	 */
	private static List<String> separateText(String text) {
		String[] stringArray = text.split("[!.。！]");
		List<String> strings = new ArrayList<>();
		for (int i = 0; i < stringArray.length; i++) {
			strings.add(stringArray[i]);
		}
		return strings;
	}

	/*
	 * topicsとrolesを初期化
	 */
	private void initTopicsAndRoles(String key) {
		topics.put(key, Topic.UNTAG);
		roles.put(key, Role.NOTROLE);
	}
}
